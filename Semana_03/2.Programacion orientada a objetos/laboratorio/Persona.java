package laboratorio;


public class Persona {
        String nombre;
        String apellidos;
        String númeroDocumentoIdentidad;
        int añoNacimiento;
        String paisNacimiento;
        char arrayGenero;
    
        Persona(String nombre, String apellidos, String númeroDocumentoIdentidad, int añoNacimiento, String paisNacimiento, char arrayGenero) {
            this.añoNacimiento = añoNacimiento;
            this.nombre = nombre;
            this.apellidos = apellidos;
            this.númeroDocumentoIdentidad = númeroDocumentoIdentidad;
            this.paisNacimiento = paisNacimiento;
            this.arrayGenero = arrayGenero ;
        }
        void imprimir(){
            System.out.println("Nombre "+nombre);  
            System.out.println("Apellidos "+apellidos);
            System.out.println("DNI "+númeroDocumentoIdentidad);
            System.out.println("Año de nacimiento "+añoNacimiento);
            System.out.println("Pais "+ paisNacimiento);
            System.out.println("Genero "+ arrayGenero);
            System.out.println("");
        }
        public static void main(String[] args) {
            Persona persona01 = new Persona("yeison", "alca", "20485625", 2003, "Perú", 'H');
            Persona persona02 = new Persona("nayeli", "Llano", "20448596", 2003, "Perú", 'M' );
            persona01.imprimir();
            persona02.imprimir();
        }
    }
