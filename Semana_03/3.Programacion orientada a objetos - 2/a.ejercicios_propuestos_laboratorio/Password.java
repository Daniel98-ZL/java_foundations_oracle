import java.io.Console;
public class Password {
    public void OcultarPassword() {
        Console console = System.console();
        if (console == null) {
            System.out.println("No se pudo obtener la instancia de la consola");
            System.exit(0);
        }

        char[] passwordArray = console.readPassword("Contraseña: ");
        for (int i = 0; i < passwordArray.length; i++) {
            System.out.print("*");
        }
        System.out.println();
        console.printf("Contraseña ingresada: \n", new String(passwordArray));

    }

    public static void main(String[] args) {
        new Password().OcultarPassword();
    }
}
