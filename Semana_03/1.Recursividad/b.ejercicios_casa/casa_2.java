public class casa_2 {
    
    public static int maximo(int[] array, int n) {
        if (n == 1) {
            return array[0];
        } else {
            int max = maximo(array, n - 1);
            return Math.max(max, array[n - 1]);
        }
    }
    public static void main(String[] args) {
        
    }
}
