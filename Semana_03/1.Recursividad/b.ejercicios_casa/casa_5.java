public class casa_5 {
    public static void dibujarTriangulo(int n, int espacios) {
        if (n == 0) {
            return;
        } else {
            dibujarTriangulo(n - 1, espacios + 1);
            for (int i = 0; i < espacios; i++) {
                System.out.print(" ");
            }
            for (int i = 0; i < n; i++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }
    public static void main(String[] args) {
        
    }
}
