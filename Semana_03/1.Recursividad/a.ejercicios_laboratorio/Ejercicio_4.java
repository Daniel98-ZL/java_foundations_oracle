import java.util.Scanner;

//Calcular el número de permutaciones de N elementos de una lista.
public class Ejercicio_4 {
    public static void main(String[] args) {
        

        Scanner entrada=new Scanner(System.in);
        System.out.println("ingrese los datos de su lista");
        String[] elementos = entrada.nextLine().split(",");
        int n,r;
        System.out.println("Ingrese valor de n");
        n=entrada.nextInt();
        r=elementos.length;
        Perm2(elementos, "", n, r);
        entrada.close();
    }

    private static void Perm2(String[] elem, String act, int n, int r) {
        if (n == 0) {
            System.out.println(act);
        } else {
            for (int i = 0; i < r; i++) {
                if (!act.contains(elem[i])) { 
                    Perm2(elem, act + elem[i] + ", ", n - 1, r);
                }
            }
        }
    }
}
