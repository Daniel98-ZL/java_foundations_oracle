public class Ejercicio_5 {
    private String type;
    public Ejercicio_5(String type, int depth ) {
        this.type = type;
    }
    
    public void draw() {
        if (type.equals("Koch")) {
            
            drawKoch();
        } else if (type.equals("Sierpinski")) {
            
            drawSierpinski();
        } else {
            // Manejar errores
            System.out.println("Tipo de fractal no reconocido.");
        }
    }
    
    private void drawKoch() {
    }
    
    private void drawSierpinski() {
        
    }
}
