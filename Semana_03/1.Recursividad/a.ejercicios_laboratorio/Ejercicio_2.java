//Calcular el máximo común divisor de N números.
import java.util.Scanner;
public class Ejercicio_2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("ingrese dos números entero para calcular el máximo común divisor");
        int numero = sc.nextInt();
        int numero2 = sc.nextInt();
        
        int mcdRecursivo = maximoComunDivisorRecursivo(numero, numero2);
        sc.close();
        System.out.println("el MCD (con recursividad es) " + mcdRecursivo);
    }

    public static int maximoComunDivisorRecursivo(int x , int y) {
        if (y == 0) return x; {
            return maximoComunDivisorRecursivo(y, x % y);
        }
    }
}
