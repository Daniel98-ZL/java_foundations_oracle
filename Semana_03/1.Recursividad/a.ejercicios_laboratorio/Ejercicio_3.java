//Calcular el mínimo común múltiplo de N números.
import java.util.Scanner;
public class Ejercicio_3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
 
        System.out.println("Dame el primer numero");
        int num1 = sc.nextInt();
 
        System.out.println("Dame el segundo numero");
        int num2 = sc.nextInt();
 
        int res = mcm(num1, num2);
        sc.close();
 
        System.out.println("El MCM: " + res);
 
    }
 
    public static int mcm(int num1, int num2) {
        int a = Math.max(num1, num2);
        int b = Math.min(num1, num2);
 
        int resultado = (a / mcd(num1, num2)) * b;
         
        return resultado;
 
    }
    
 
    public static int mcd(int num1, int num2) {
 
        int a = Math.max(num1, num2);
        int b = Math.min(num1, num2);
 
        int resultado = 0;
        do {
            resultado = b;
            b = a % b;
            a = resultado;
 
        } while (b != 0);
 
        return resultado;
 
    }
}

