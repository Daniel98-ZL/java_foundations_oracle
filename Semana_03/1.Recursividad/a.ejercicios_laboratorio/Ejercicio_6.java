import java.io.*;
import java.util.*;
public class Ejercicio_6 {
    public static void main(String[] args) throws IOException {
        Random rnd = new Random();
        FileWriter writer = new FileWriter("datos_generados.txt");
        for (int i = 0; i < 1000; i++) {
            int dato = rnd.nextInt(301);
            writer.write(Integer.toString(dato) + "\n");
        }
        writer.close();

        Scanner scanner = new Scanner(new File("datos_generados.txt"));
        int[] datos = new int[1000];
        int suma = 0;
        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;
        int moda = -1;
        int modaFrecuencia = -1;
        Map<Integer, Integer> frecuencias = new HashMap<>();
        while (scanner.hasNextLine()) {
            int dato = Integer.parseInt(scanner.nextLine());
            datos[suma] = dato;
            suma += dato;
            if (dato > max) {
                max = dato;
            }
            if (dato < min) {
                min = dato;
            }
            int frecuencia = frecuencias.getOrDefault(dato, 0) + 1;
            frecuencias.put(dato, frecuencia);
            if (frecuencia > modaFrecuencia) {
                modaFrecuencia = frecuencia;
                moda = dato;
            }
        }
        scanner.close();
        double promedio = (double) suma / 1000;
        double desviacion = Math.sqrt(Arrays.stream(datos).mapToDouble(d -> Math.pow(d - promedio, 2)).sum() / 1000);

        FileWriter writer2 = new FileWriter("resultados.txt");
        writer2.write("Valor: " + suma + "\n");
        writer2.write("Máximo: " + max + "\n");
        writer2.write("Mínimo: " + min + "\n");
        writer2.write("Promedio: " + promedio + "\n");
        writer2.write("Moda: " + moda + " (frecuencia: " + modaFrecuencia + ")\n");
        writer2.write("Desviación estándar: " + desviacion + "\n");
        writer2.close();
    }
}
