public class Ejercicio_8 {
    private String nombreCompleto;
    private String grupoPersonas;

    public Ejercicio_8(String nombreCompleto, String grupoPersonas) {
        this.nombreCompleto = nombreCompleto;
        this.grupoPersonas = grupoPersonas;
    }

    public String getNombreCompleto() {
        return nombreCompleto;
    }

    public void setNombreCompleto(String nombreCompleto) {
        this.nombreCompleto = nombreCompleto;
    }

    public String getGrupoPersonas() {
        return grupoPersonas;
    }

    public void setGrupoPersonas(String grupoPersonas) {
        this.grupoPersonas = grupoPersonas;
    }

    @Override
    public String toString() {
        return "Persona{" +
                "nombreCompleto='" + nombreCompleto + '\'' +
                ", grupoPersonas='" + grupoPersonas + '\'' +
                '}';
    }
    public class Trabajador extends Ejercicio_8 {
        private String profesion;
    
        public Trabajador(String nombreCompleto, String grupoPersonas, String profesion) {
            super(nombreCompleto, grupoPersonas);
            this.profesion = profesion;
        }
    
        public String getProfesion() {
            return profesion;
        }
    
        public void setProfesion(String profesion) {
            this.profesion = profesion;
        }
    
        @Override
        public String toString() {
            return "Trabajador{" +
                    "nombreCompleto='" + getNombreCompleto() + '\'' +
                    ", grupoPersonas='" + getGrupoPersonas() + '\'' +
                    ", profesion='" + profesion + '\'' +
                    '}';
        }
    }
}