//Calcular el área de un triángulo a partir de sus tres lados.
import java.util.Scanner;
public class Ejercicio_4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese los tres lados de su triangulo");
        double lado1 = sc.nextDouble();
        double lado2 = sc.nextDouble();
        double lado3 = sc.nextDouble();
        sc.close();
        double perimetro = (lado1 + lado2 + lado3) / 2;
        double area = Math.sqrt((perimetro*(perimetro-lado1)*(perimetro-lado2)*(perimetro-lado3)));
        System.out.println("El area de su triangulo es: " + area);
    }
}
