//Escriba un programa que realice la conversión de grados Celsius (°C) a grados Fahrenheit (°F). La ecuación de conversión es: F=95∗C+32F = 95*C + 32F=95∗C+32
import java.util.Scanner;
public class Ejercicio__13 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese el numero de grados Celcius");
        double C = sc.nextDouble();
        double F = (9 * C/5) + 32;
        sc.close();
        System.out.println("la conversion a Fahrenheit es: " + F);
    }
}
