//Determine el área de la superficie lateral y el volumen de un cilindro conocido su radio y altura.
import java.util.Scanner;
public class Ejercicio__14 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese el radio de su cilindro");
        double radio = sc.nextDouble();
        System.out.println("ingrese la altura de su cilindro");
        double altura = sc.nextDouble();
        sc.close();
        double superficie = 2 * Math.PI * radio * altura;
        double volumen = Math.PI * Math.pow(radio, 2) * altura;
        System.out.println("La superficie lateral de su cilindro es: " + superficie);
        System.out.println("El volumen de su cilindro es: " + volumen);
    }
}
