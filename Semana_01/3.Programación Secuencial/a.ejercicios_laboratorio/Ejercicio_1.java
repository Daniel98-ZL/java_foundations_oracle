//Realizar un programa que calcule el area y perímetro de una elipse.
import java.util.Scanner;
public class Ejercicio_1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("ingrese las dos longitudes de sus radios");
        double longitud1 = sc.nextDouble();
        double longitud2 = sc.nextDouble();
        double area = Math.PI *longitud1*longitud2;
        double perimetro = Math.PI* (longitud1 + longitud2);
        sc.close();
        System.out.println("El area de su elipse es: " + area);
        System.out.println("El perimetro de su elipse es: " + perimetro);
    }
}
