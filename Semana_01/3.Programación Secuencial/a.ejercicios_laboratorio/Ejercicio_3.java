//Calcular el valor de la hipotenusa de un triángulo rectángulo a partir de sus dos catetos.
import java.util.Scanner;
public class Ejercicio_3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("ingrese los dos catetos de su triangulo rectangulo");
        double cateto1 = sc.nextDouble();
        double cateto2 = sc.nextDouble();
        sc.close();
        double hipotenusa = Math.hypot(cateto1, cateto2);
        System.out.println("la hipotenusa de su triangulo rectangulo es: " + hipotenusa);
    }
}
