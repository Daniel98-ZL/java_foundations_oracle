import java.util.Scanner;
public class Ejercicio__18 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("ingrese el total de su compra");
        double compra = sc.nextDouble();
        sc.close();
        double descuento = compra * 0.15;
        double pagoFinal = compra - descuento;
        System.out.println("Usted tiene un descuento del 15% en total tiene que pagar " + pagoFinal);
    }
}
