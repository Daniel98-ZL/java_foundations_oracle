import java.util.Scanner;

//package Semana_01.3.Programación Secuencial.b.ejercicios_casa;

public class Tarea_20 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Ingrese el primer número: ");
        int num1 = input.nextInt();

        System.out.print("Ingrese el segundo número: ");
        int num2 = input.nextInt();
        input.close();
        if (sonAmigos(num1, num2)) {
            System.out.println(num1 + " y " + num2 + " son números amigos.");
        } else {
            System.out.println(num1 + " y " + num2 + " no son números amigos.");
        }
    }

    public static boolean sonAmigos(int num1, int num2) {
        int sumDivisoresNum1 = sumDivisores(num1);
        int sumDivisoresNum2 = sumDivisores(num2);

        return (sumDivisoresNum1 == num2 && sumDivisoresNum2 == num1);
    }

    public static int sumDivisores(int num) {
        int sum = 0;

        for (int i = 1; i <= num/2; i++) {
            if (num % i == 0) {
                sum += i;
            }
        }

        return sum;
    }
}
