import java.util.Scanner;

//package Semana_01.3.Programación Secuencial.b.ejercicios_casa;

public class Tarea_14 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
    
        System.out.print("Ingresa el precio de costo del producto: ");
        double precioCosto = input.nextDouble();
    
        System.out.print("Ingresa el porcentaje de ganancia: ");
        double porcentajeGanancia = input.nextDouble();
    
        System.out.print("Ingresa el porcentaje de impuestos: ");
        double porcentajeImpuestos = input.nextDouble();
        input.close();
        double precioVenta = precioCosto * (1 + porcentajeGanancia / 100) * (1 + porcentajeImpuestos / 100);
    
        System.out.println("El precio final de venta es: " + precioVenta);
      }
}
