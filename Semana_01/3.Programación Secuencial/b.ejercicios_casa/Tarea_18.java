import java.util.Scanner;

//package Semana_01.3.Programación Secuencial.b.ejercicios_casa;

public class Tarea_18 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
    
        System.out.print("Ingresa un número para mostrar su tabla de multiplicar: ");
        int numero = input.nextInt();
        input.close();
        for (int i = 1; i <= 10; i++) {
          System.out.println(numero + " x " + i + " = " + (numero * i));
        }
    }
}
