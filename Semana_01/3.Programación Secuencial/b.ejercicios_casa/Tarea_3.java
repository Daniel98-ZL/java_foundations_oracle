import java.util.Scanner;

public class Tarea_3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
    
        System.out.print("Ingresa un número entero: ");
        int numero = input.nextInt();
    
        int doble = numero * 2;
        int triple = numero * 3;
        input.close();
        System.out.println("El doble de " + numero + " es: " + doble);
        System.out.println("El triple de " + numero + " es: " + triple);
      }
}
