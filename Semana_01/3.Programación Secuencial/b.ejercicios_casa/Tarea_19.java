import java.util.Scanner;

//package Semana_01.3.Programación Secuencial.b.ejercicios_casa;

public class Tarea_19 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int count = 0;

        System.out.print("Ingrese un número (ingrese un número negativo para detener): ");
        int number = input.nextInt();

        while (number >= 0) {
            if (number % 10 == 2) {
                count++;
            }
            System.out.print("Ingrese otro número: ");
            number = input.nextInt();
        }
        input.close();
        System.out.println("Se ingresaron " + count + " números que terminan en 2.");
    }
}
