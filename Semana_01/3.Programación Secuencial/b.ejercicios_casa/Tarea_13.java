import java.util.Scanner;

public class Tarea_13 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
    
        System.out.print("Ingresa tu fecha de nacimiento (DD/MM/AAAA): ");
        String fechaNacimiento = input.next();
        input.close();
        fechaNacimiento = fechaNacimiento.replaceAll("/", "");
    
        int sumaCifras = 0;
        for (int i = 0; i < fechaNacimiento.length(); i++) {
          sumaCifras += Integer.parseInt(fechaNacimiento.substring(i, i+1));
        }
    
        int numeroSuerte = sumaCifras;
        while (numeroSuerte > 9) {
          int sumaCifrasResultado = 0;
          String cadenaNumero = Integer.toString(numeroSuerte);
          for (int i = 0; i < cadenaNumero.length(); i++) {
            sumaCifrasResultado += Integer.parseInt(cadenaNumero.substring(i, i+1));
          }
          numeroSuerte = sumaCifrasResultado;
        }
    
        System.out.println("Tu número de la suerte es: " + numeroSuerte);
      }
}
