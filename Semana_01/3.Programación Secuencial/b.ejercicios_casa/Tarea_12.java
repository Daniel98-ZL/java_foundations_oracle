import java.util.Scanner;

public class Tarea_12 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
    
        System.out.print("Ingresa un número de 5 cifras: ");
        int numero = input.nextInt();
    
        String cadenaNumero = Integer.toString(numero);
        int longitud = cadenaNumero.length();
        input.close();
        for (int i = longitud - 1; i >= 0; i--) {
          System.out.println("La cifra " + (longitud - i) + " es: " + cadenaNumero.charAt(i));
        }
      }
}
