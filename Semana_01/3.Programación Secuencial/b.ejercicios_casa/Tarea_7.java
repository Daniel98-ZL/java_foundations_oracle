import java.util.Scanner;

public class Tarea_7 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
    
        System.out.print("Ingresa la longitud del primer cateto: ");
        double cateto1 = input.nextDouble();
    
        System.out.print("Ingresa la longitud del segundo cateto: ");
        double cateto2 = input.nextDouble();
        input.close();
        double hipotenusa = Math.sqrt(cateto1 * cateto1 + cateto2 * cateto2);
    
        System.out.println("La longitud de la hipotenusa es: " + hipotenusa);
      }
}
