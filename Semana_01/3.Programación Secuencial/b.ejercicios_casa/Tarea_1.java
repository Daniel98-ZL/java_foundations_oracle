import java.util.Scanner;

public class Tarea_1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
    
        System.out.print("Ingresa el primer número entero: ");
        int numero1 = input.nextInt();
    
        System.out.print("Ingresa el segundo número entero: ");
        int numero2 = input.nextInt();
        input.close();
        System.out.println("El primer número que ingresaste es: " + numero1);
        System.out.println("El segundo número que ingresaste es: " + numero2);
      }
}
