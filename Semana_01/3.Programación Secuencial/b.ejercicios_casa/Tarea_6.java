import java.util.Scanner;

public class Tarea_6 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
    
        System.out.print("Ingresa una velocidad en kilómetros por hora: ");
        double kmh = input.nextDouble();
        input.close();
        double ms = kmh / 3.6;
    
        System.out.println(kmh + " kilómetros por hora equivale a " + ms + " metros por segundo.");
      }
}
