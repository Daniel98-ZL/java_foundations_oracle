import java.util.Scanner;

public class Tarea_11 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
    
        System.out.print("Ingresa un número de 5 cifras: ");
        int numero = input.nextInt();
    
        String cadenaNumero = Integer.toString(numero);
        int longitud = cadenaNumero.length();
        input.close();
        for (int i = 0; i < longitud; i++) {
          System.out.println("La cifra " + (i + 1) + " es: " + cadenaNumero.charAt(i));
        }
      }
}
