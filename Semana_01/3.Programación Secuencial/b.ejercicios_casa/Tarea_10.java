import java.util.Scanner;

public class Tarea_10 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
    
        System.out.print("Ingresa un número de 3 cifras: ");
        int numero = input.nextInt();
    
        int cifra1 = numero / 100;
        int cifra2 = (numero / 10) % 10;
        int cifra3 = numero % 10;
        input.close();
        System.out.println("La primera cifra es: " + cifra1);
        System.out.println("La segunda cifra es: " + cifra2);
        System.out.println("La tercera cifra es: " + cifra3);
      }
}
