import java.util.Scanner;

//package Semana_01.3.Programación Secuencial.b.ejercicios_casa;

public class Tarea_15 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
    
        System.out.print("Ingresa el número: ");
        int numero = input.nextInt();
        System.out.print("Ingresa la cantidad de cifras a quitar: ");
        int m = input.nextInt();
        input.close();
        int factor = (int) Math.pow(10, m);
        int numeroSinMCifras = numero / factor * factor;
    
        System.out.println("El número sin las últimas " + m + " cifras es: " + numeroSinMCifras);
      }
}
