import java.util.Scanner;

//package Semana_01.3.Programación Secuencial.b.ejercicios_casa;

public class Tarea_16 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
    
        System.out.print("Ingresa la temperatura en grados Celsius: ");
        double celsius = input.nextDouble();
        input.close();
        
        double kelvin = celsius + 273.15;
        double reamur = celsius * 4/5;
    
        System.out.println("La temperatura en grados Kelvin es: " + kelvin);
        System.out.println("La temperatura en grados Reamur es: " + reamur);
      }
}
