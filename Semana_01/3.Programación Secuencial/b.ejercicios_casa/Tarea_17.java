import java.util.Scanner;

//package Semana_01.3.Programación Secuencial.b.ejercicios_casa;

public class Tarea_17 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        char respuesta;
        
        do {
          System.out.print("Ingresa la temperatura en grados Celsius: ");
          double celsius = input.nextDouble();
    
          double kelvin = celsius + 273.15;
    
          System.out.println("La temperatura en grados Kelvin es: " + kelvin);
    
          System.out.print("Repetir proceso? (S/N): ");
          respuesta = input.next().charAt(0);
          input.close();
        } while (respuesta == 'S');
      }
}
