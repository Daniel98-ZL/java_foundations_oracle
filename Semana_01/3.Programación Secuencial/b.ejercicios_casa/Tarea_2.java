import java.util.Scanner;

public class Tarea_2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
    
        System.out.print("Ingresa tu nombre: ");
        String nombre = input.nextLine();
        input.close();
        System.out.println("¡Buenos días, " + nombre + "!");
      }
}
