import java.util.Scanner;

public class Tarea_4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
    
        System.out.print("Ingresa una temperatura en grados centígrados: ");
        double celsius = input.nextDouble();
        input.close();
        double fahrenheit = 32 + (9 * celsius / 5);
    
        System.out.println(celsius + " grados Celsius equivale a " + fahrenheit + " grados Fahrenheit.");
      }
}
