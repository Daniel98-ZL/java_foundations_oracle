import java.util.Scanner;

public class Tarea_5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
    
        System.out.print("Ingresa el valor del radio de la circunferencia: ");
        double radio = input.nextDouble();
    
        double longitud = 2 * Math.PI * radio;
        double area = Math.PI * radio * radio;
        input.close();
        System.out.println("La longitud de la circunferencia es: " + longitud);
        System.out.println("El área de la circunferencia es: " + area);
      }
}
