import java.util.Scanner;

public class Tarea_8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
    
        System.out.print("Ingresa el radio de la esfera: ");
        double radio = input.nextDouble();
        input.close();
        double volumen = 4.0 / 3.0 * Math.PI * Math.pow(radio, 3);
    
        System.out.println("El volumen de la esfera es: " + volumen);
      }
}
