import java.util.Scanner;
public class Tarea_015 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese una lista de palabras separadas por comas: ");
        String input = scanner.nextLine();
        String[] words = input.split(",");
        int count = 0;
        scanner.close();
        for (String word : words) {
            if (word.length() > 1 && word.charAt(0) == word.charAt(word.length()-1)) {
                count++;
            }
        }
        System.out.println("Hay " + count + " palabras con la misma letra al principio y al final.");
    }
}
