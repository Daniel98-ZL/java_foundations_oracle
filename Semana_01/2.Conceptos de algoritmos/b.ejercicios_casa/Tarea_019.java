import java.util.*;

public class Tarea_019 {
    public static void main(String[] args) {
        List<String> palabras = Arrays.asList("Hola", "Mundo", "Java", "Programacion", "Palabra");

        int contador = 0;
        for (String palabra : palabras) {
            Set<Character> caracteres = new HashSet<>();
            for (int i = 0; i < palabra.length(); i++) {
                caracteres.add(palabra.charAt(i));
            }
            if (caracteres.size() == palabra.length()) {
                contador++;
            }
        }
        System.out.println("Cantidad de palabras con letras diferentes: " + contador);
    }
}
