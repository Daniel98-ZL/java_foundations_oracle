public class Tarea_07 {
    public static void main(String[] args) {
        String[] words = {"hellooooo", "world", "bookkeeper", "pepper", "committee", "success", "address"};

        int count = 0;

        for (String word : words) {
            for (int i = 2; i < word.length(); i++) {
                char c1 = word.charAt(i);
                char c2 = word.charAt(i - 1);
                char c3 = word.charAt(i - 2);
                if (c1 == c2 && c2 == c3) {
                    count++;
                    break;
                }
            }
        }

        System.out.println("Número de palabras con la misma letra repetida tres veces consecutivas: " + count);
    }
}
