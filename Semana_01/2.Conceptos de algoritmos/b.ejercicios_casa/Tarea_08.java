public class Tarea_08 {
    public static void main(String[] args) {
        int sumOfDigits = 0;
        for (int i = 1; i <= 100; i++) {
            int number = i;
            int digitSum = 0;
            while (number > 0) {
                digitSum += number % 10;
                number /= 10;
            }
            sumOfDigits += digitSum;
        }

        for (int i = 1; i <= 100; i++) {
            if (i % sumOfDigits == 0) {
                System.out.println(i + " es divisible por la suma de los dígitos de todos los números del 1 al 100");
                break;
            }
        }
    }
}
