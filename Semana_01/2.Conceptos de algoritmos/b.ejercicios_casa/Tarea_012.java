public class Tarea_012 {
    public static void main(String[] args) {
        int[] numbers = {4, 22, 27, 85, 94};

        int count = 0;
        for (int number : numbers) {
            if (isSmithNumber(number)) {
                count++;
            }
        }

        System.out.println("Número de números de Smith: " + count);
    }

    public static boolean isSmithNumber(int number) {
        int digitSum = getDigitSum(number);
        int factorizationSum = getFactorizationSum(number);
        return digitSum == factorizationSum;
    }

    public static int getDigitSum(int number) {
        int sum = 0;
        while (number > 0) {
            sum += number % 10;
            number /= 10;
        }
        return sum;
    }

    public static int getFactorizationSum(int number) {
        int sum = 0;
        for (int i = 2; i <= number; i++) {
            while (number % i == 0) {
                sum += getDigitSum(i);
                number /= i;
            }
        }
        return sum;
    }
}
