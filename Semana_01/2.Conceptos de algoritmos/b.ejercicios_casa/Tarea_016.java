import java.util.List;
import java.util.ArrayList;
public class Tarea_016 {
    public static void main(String[] args) {
        List<Integer> numbers = new ArrayList<>();
        numbers.add(153);
        numbers.add(371);
        numbers.add(9474);
        numbers.add(9475);

        int count = 0;

        for (int num : numbers) {
            int temp = num;
            int sum = 0;
            int digits = String.valueOf(num).length();

            while (temp > 0) {
                int digit = temp % 10;
                sum += Math.pow(digit, digits);
                temp /= 10;
            }

            if (sum == num) {
                count++;
            }
        }

        System.out.println(count + " los números son números de Armstrong. ");
    }
}
