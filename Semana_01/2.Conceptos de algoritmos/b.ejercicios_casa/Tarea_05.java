public class Tarea_05 {
    public static void main(String[] args) {
        String[] words = {"hello", "world", "aeiou", "quick", "brown", "fox", "jumps", "over", "the", "lazy", "dog"};

        int count = 0;

        for (String word : words) {
            if (word.contains("a") && word.contains("e") && word.contains("i") && word.contains("o") && word.contains("u")) {
                count++;
            }
        }

        System.out.println("Número de palabras con todas las vocales:" + count);
    }
}
