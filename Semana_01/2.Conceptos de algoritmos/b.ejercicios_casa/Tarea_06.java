public class Tarea_06 {
    public static void main(String[] args) {
        int[] numbers = {232792560, 232792561};

        for (int num : numbers) {
            boolean isDivisible = true;
            for (int i = 1; i <= 20; i++) {
                if (num % i != 0) {
                    isDivisible = false;
                    break;
                }
            }
            if (isDivisible) {
                System.out.println(num + " es divisible por todos los números del 1 al 20. ");
                break;
            }
        }
    }
}
