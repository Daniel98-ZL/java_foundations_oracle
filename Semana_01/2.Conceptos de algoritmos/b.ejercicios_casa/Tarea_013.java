import java.util.*;
public class Tarea_013 {
    public static void main(String[] args) {
    
        String[] palabras = {"hola", "adios", "huevo", "murcielago", "programacion", "computadora"};

        int contador = 0;
        for (String palabra : palabras) {
            Map<Character, Integer> letras = new HashMap<>();
            
            for (int i = 0; i < palabra.length(); i++) {
                char letra = palabra.charAt(i);
                letras.put(letra, letras.getOrDefault(letra, 0) + 1);
            }
            
            boolean encontrado = false;
            for (int ocurrencias : letras.values()) {
                if (ocurrencias > 2 && ocurrencias < 5) {
                    encontrado = true;
                    break;
                }
            }
            if (encontrado) {
                contador++;
            }
        }
        System.out.println("Hay " + contador + " palabras con una letra que aparece más de dos veces pero menos de cinco veces.");
    }
}
