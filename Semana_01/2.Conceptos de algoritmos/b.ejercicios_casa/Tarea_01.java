import java.util.*;
public class Tarea_01 {
    public static void main(String[] args) {
        int[] nums = {1, 2, 3, 4, 5, 6, 7};
        int targetSum = 6;

        Set<Integer> set = new HashSet<>();
        for (int num : nums) {
            int complement = targetSum - num;
            if (set.contains(complement)) {
                System.out.println("Suma del número determinado que es 6: (" + num + ", " + complement + ")");
            }
            set.add(num);
        }
    }
}
