public class Tarea_011 {
    public static void main(String[] args) {
        String[] words = {"hello", "world", "apple", "banana", "avocado"};

        int count = 0;
        for (String word : words) {
            if (word.length() >= 4 && word.charAt(0) == word.charAt(3)) {
                count++;
            }
        }

        System.out.println("Número de palabras con la misma letra en las posiciones 1 y 4: " + count);
    }
}
