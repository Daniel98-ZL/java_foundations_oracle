public class Tarea_09 {
    public static void main(String[] args) {
        String[] words = {"hello", "world", "Java", "programming", "language", "odd", "even"};

        int count = 0;
        for (String word : words) {
            int[] letterCount = new int[26];
        for (int i = 0; i < word.length(); i++) {
                char c = word.charAt(i);
                letterCount[c - 'a']++;
            }
        boolean hasOddCount = false;
        for (int counta : letterCount) {
            if (counta % 2 != 0) {
                hasOddCount = true;
                break;
            }
        }
        if (hasOddCount) {
            count++;
        }
    }

    System.out.println("Number of words with an odd number of repeated letters: " + count);
    }
}
