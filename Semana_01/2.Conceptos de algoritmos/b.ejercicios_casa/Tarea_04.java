public class Tarea_04 {
    public static void main(String[] args) {
        int[] numbers = {2520, 2521, 2522, 2523, 2524, 2525, 2526, 2527, 2528, 2529};

        int count = 0;

        for (int num : numbers) {
            boolean isDivisible = true;
            for (int i = 1; i <= 10; i++) {
                if (num % i != 0) {
                    isDivisible = false;
                    break;
                }
            }
            if (isDivisible) {
                count++;
            }
        }

        System.out.println("Número de números divisibles por todos los números del 1 al 10: " + count);
    }
}
