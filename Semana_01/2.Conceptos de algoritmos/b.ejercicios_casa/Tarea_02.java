public class Tarea_02 {
    public static void main(String[] args) {
        String[] words = {"holaaaaa", "senatinos", "bienvedidos ", "a sus", "clases ", "de java"};

        int count = 0;
        for (String word : words) {
            int consecutiveVowels = 0;
            for (int i = 0; i < word.length(); i++) {
                char c = Character.toLowerCase(word.charAt(i));
                if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u') {
                    consecutiveVowels++;
                    if (consecutiveVowels >= 3) {
                        count++;
                        break;
                    }
                } else {
                    consecutiveVowels = 0;
                }
            }
        }

        System.out.println("Número de palabras con más de dos vocales consecutivas: " + count);
    }
}
