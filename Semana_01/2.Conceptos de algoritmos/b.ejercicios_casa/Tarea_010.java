public class Tarea_010 {
    public static void main(String[] args) {
        int[] numbers = {145, 23, 40585, 123, 1450};

        int count = 0;
        for (int num : numbers) {
            int sumFactorialDigits = 0;
            int n = num;
            while (n > 0) {
                int digit = n % 10;
                sumFactorialDigits += factorial(digit);
                n /= 10;
            }
            if (sumFactorialDigits == num) {
                count++;
            }
        }

        System.out.println("Número de números con la propiedad deseada: " + count);
    }

    private static int factorial(int n) {
        if (n == 0) {
            return 1;
        } else {
            return n * factorial(n - 1);
        }
    }
}
