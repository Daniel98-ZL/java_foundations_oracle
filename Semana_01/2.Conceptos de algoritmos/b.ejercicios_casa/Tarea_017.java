import java.util.ArrayList;
import java.util.Scanner;

public class Tarea_017 {
    public static void main(String[] args) {
       
        ArrayList<String> palabras = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese el número de palabras en la lista: ");
        int numPalabras = scanner.nextInt();
        System.out.println("Ingrese las palabras:");
        for (int i = 0; i < numPalabras; i++) {
            palabras.add(scanner.next());
        }
        scanner.close();

       
        int contador = 0;
        for (String palabra : palabras) {
            for (int i = 0; i < palabra.length(); i++) {
                char letra = palabra.charAt(i);
                int cantidad = 0;
                for (int j = 0; j < palabra.length(); j++) {
                    if (palabra.charAt(j) == letra) {
                        cantidad++;
                    }
                }
                if (cantidad == 2) {
                    contador++;
                    break;
                }
            }
        }

        
        System.out.println("Hay " + contador + " palabras que tienen una letra que aparece exactamente dos veces en cualquier posición.");
    }
}
