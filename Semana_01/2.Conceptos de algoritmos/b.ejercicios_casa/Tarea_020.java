import java.util.*;
public class Tarea_020 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Ingrese los números enteros separados por espacios: ");
        String numerosStr = input.nextLine();
        String[] numerosArr = numerosStr.split(" ");

        int narcisismos = 0;
        input.close();
        for (String numeroStr : numerosArr) {
            int numero = Integer.parseInt(numeroStr);
            int numDigitos = String.valueOf(numero).length();
            int sumaPotencias = 0;

            for (int i = 0; i < numDigitos; i++) {
                int digito = Integer.parseInt(String.valueOf(numeroStr.charAt(i)));
                sumaPotencias += Math.pow(digito, numDigitos);
            }

            if (sumaPotencias == numero) {
                narcisismos++;
            }
        }

        System.out.println("Hay " + narcisismos + " números de narcisismo en la lista.");
    }
}
