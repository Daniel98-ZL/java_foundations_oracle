public class Tarea_03 {
    public static void main(String[] args) {
        int[] numbers = {6, 7, 8, 9, 10, 11, 12, 13, 14, 15};

        int perfectCount = 0;
        int fibonacciCount = 0;

        for (int num : numbers) {
            if (isPerfect(num)) {
                perfectCount++;
            }
            if (isFibonacci(num)) {
                fibonacciCount++;
            }
        }

        System.out.println("Número de números perfectos: " + perfectCount);
        System.out.println("Número de números de Fibonacci: " + fibonacciCount);
    }

    private static boolean isPerfect(int num) {
        int sum = 0;
        for (int i = 1; i <= num / 2; i++) {
            if (num % i == 0) {
                sum += i;
            }
        }
        return sum == num;
    }

    private static boolean isFibonacci(int num) {
        if (num == 0 || num == 1) {
            return true;
        }
        int prev = 1;
        int curr = 1;
        while (curr < num) {
            int temp = curr;
            curr = curr + prev;
            prev = temp;
        }
        return curr == num;
    }
}
