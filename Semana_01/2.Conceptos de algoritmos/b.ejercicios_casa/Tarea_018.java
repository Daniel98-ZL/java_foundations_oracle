import java.util.Scanner;

public class Tarea_018 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Ingrese una lista de números enteros separados por comas: ");
        String[] numbers = input.nextLine().split(",");
        int count = 0;
        input.close();
        for (String number : numbers) {
            int sum = 0;
            int n = Integer.parseInt(number);

            
            while (n > 0) {
                sum += n % 10;
                n /= 10;
            }

            
            if (Integer.parseInt(number) % sum == 0) {
                count++;
            }
        }

        System.out.println("Hay " + count + " números de Harshad en la lista.");
    }
}
