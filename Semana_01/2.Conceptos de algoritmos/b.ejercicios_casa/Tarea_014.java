import java.util.*;
public class Tarea_014 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese los números separados por espacios: ");
        
        String input = scanner.nextLine();
        String[] numbers = input.split(" ");
        int count = 0;
        scanner.close();
        for (String number : numbers) {
            int n = Integer.parseInt(number);
            int square = n * n;
            String squareStr = String.valueOf(square);
            int mid = squareStr.length() / 2;
            int left = Integer.parseInt(squareStr.substring(0, mid));
            int right = Integer.parseInt(squareStr.substring(mid));
            if (left + right == n) {
                count++;
            }
        }
        System.out.println("Hay " + count + " números de Kaprekar en la lista.");
    }
}
