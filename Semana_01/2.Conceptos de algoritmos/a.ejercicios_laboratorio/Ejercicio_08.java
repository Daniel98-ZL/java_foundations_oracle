import java.util.Scanner;

public class Ejercicio_08 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese una fecha en formato dd/mm/yyyy: ");
        String fechaStr = sc.nextLine();
        sc.close();
        if (esFechaValida(fechaStr)) {
            System.out.println("La fecha ingresada es válida.");
        } else {
            System.out.println("La fecha ingresada no es válida.");
        }
    }

    public static boolean esFechaValida(String fechaStr) {
        String[] fechaArr = fechaStr.split("/");
        int dia = Integer.parseInt(fechaArr[0]);
        int mes = Integer.parseInt(fechaArr[1]);
        int anio = Integer.parseInt(fechaArr[2]);

        if (anio < 0) {
            return false;
        }
        if (mes < 1 || mes > 12) {
            return false;
        }
        int diasEnMes = diasEnMes(mes, anio);
        if (dia < 1 || dia > diasEnMes) {
            return false;
        }

        return true;
    }

    public static int diasEnMes(int mes, int anio) {
        switch (mes) {
            case 2:
                return esBisiesto(anio) ? 29 : 28;
            case 4:
            case 6:
            case 9:
            case 11:
                return 30;
            default:
                return 31;
        }
    }

    public static boolean esBisiesto(int anio) {
        return (anio % 4 == 0 && anio % 100 != 0) || (anio % 400 == 0);
    }
}
