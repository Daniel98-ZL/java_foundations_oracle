import java.util.Scanner;

public class Ejercicio_07 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese una cadena de caracteres: ");
        String cadena = sc.nextLine();
        sc.close();
        if (esPalindromo(cadena)) {
            System.out.println("La cadena ingresada es un palíndromo.");
        } else {
            System.out.println("La cadena ingresada no es un palíndromo.");
        }
    }

    public static boolean esPalindromo(String cadena) {
        cadena = cadena.toLowerCase().replaceAll("\\s+", "");

        StringBuilder reverso = new StringBuilder(cadena).reverse();
        return cadena.equals(reverso.toString());
    }
}
