import java.util.Scanner;

public class Ejercicio_10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese una lista de números enteros separados por espacios:");
        String input = sc.nextLine();
        sc.close();
        String[] nums = input.split(" ");
        int pares = 0;

        for (String num : nums) {
            if (esPar(Integer.parseInt(num))) {
                pares++;
            }
        }

        System.out.println("La lista ingresada contiene " + pares + " números pares.");
    }

    public static boolean esPar(int num) {
        return num % 2 == 0;
    }
}
