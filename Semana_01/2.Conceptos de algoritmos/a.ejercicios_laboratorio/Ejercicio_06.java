import java.util.Scanner;

public class Ejercicio_06 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese su peso en kilogramos: ");
        double peso = sc.nextDouble();
        System.out.println("Ingrese su estatura en metros: ");
        double estatura = sc.nextDouble();
        sc.close();
        double imc = peso / (estatura * estatura);

        System.out.println("Su índice de masa corporal (IMC) es: " + imc);
    }
}
