import java.util.Scanner;

public class Ejercicio_11 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese una lista de números enteros separados por espacios:");
        String input = sc.nextLine();
        sc.close();
        String[] nums = input.split(" ");
        int multiplos = 0;

        for (String num : nums) {
            if (esMultiploDeTres(Integer.parseInt(num))) {
                multiplos++;
            }
        }

        System.out.println("La lista ingresada contiene " + multiplos + " números múltiplos de 3.");
    }

    public static boolean esMultiploDeTres(int num) {
        return num % 3 == 0;
    }
}
