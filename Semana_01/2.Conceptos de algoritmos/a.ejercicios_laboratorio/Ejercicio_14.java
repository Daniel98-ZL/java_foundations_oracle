import java.util.ArrayList;
import java.util.Scanner;

public class Ejercicio_14 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese una lista de palabras separadas por espacios:");
        String input = sc.nextLine();
        sc.close();
        String[] palabras = input.split(" ");
        ArrayList<String> palindromos = new ArrayList<>();

        for (String palabra : palabras) {
            if (esPalindromo(palabra)) {
                palindromos.add(palabra);
            }
        }

        System.out.println("La lista ingresada contiene " + palindromos.size() + " palíndromos:");
        for (String palindromo : palindromos) {
            System.out.println(palindromo);
        }
    }

    public static boolean esPalindromo(String palabra) {
        int n = palabra.length();
        for (int i = 0; i < n / 2; i++) {
            if (palabra.charAt(i) != palabra.charAt(n - i - 1)) {
                return false;
            }
        }
        return true;
    }
}
