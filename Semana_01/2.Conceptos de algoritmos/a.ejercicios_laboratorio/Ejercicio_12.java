import java.util.Scanner;

public class Ejercicio_12 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese una lista de números enteros separados por espacios:");
        String input = sc.nextLine();
        sc.close();
        String[] nums = input.split(" ");
        int perfectos = 0;

        for (String num : nums) {
            if (esNumeroPerfecto(Integer.parseInt(num))) {
                perfectos++;
            }
        }

        System.out.println("La lista ingresada contiene " + perfectos + " números perfectos.");
    }

    public static boolean esNumeroPerfecto(int num) {
        int sum = 0;
        for (int i = 1; i <= num / 2; i++) {
            if (num % i == 0) {
                sum += i;
            }
        }
        return sum == num;
    }
}
