import java.util.ArrayList;
import java.util.Scanner;

public class Ejercicio_18 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese una lista de palabras separadas por espacios:");
        String input = sc.nextLine();

        String[] palabras = input.split(" ");
        ArrayList<String> palabrasLongitud = new ArrayList<>();

        System.out.println("Ingrese la longitud máxima para buscar:");
        int longitudMaxima = sc.nextInt();
        sc.close();
        for (String palabra : palabras) {
            if (palabra.length() < longitudMaxima) {
                palabrasLongitud.add(palabra);
            }
        }

        System.out.println("La lista ingresada contiene " + palabrasLongitud.size() + " palabras con longitud menor que " + longitudMaxima + ":");
        for (String palabra : palabrasLongitud) {
            System.out.println(palabra);
        }
    }
}
