import java.util.Scanner;

public class Ejercicio_19 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese un número entero:");
        int num = sc.nextInt();
        sc.close();
        double suma = 0.0;
        for (int i = 1; i <= num; i++) {
            suma += 1.0 / i;
        }

        if (suma == (int) suma) {
            System.out.println(num + " es un número armónico.");
        } else {
            System.out.println(num + " no es un número armónico.");
        }
    }
}
