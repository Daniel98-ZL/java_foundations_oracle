import java.util.ArrayList;
import java.util.Scanner;

public class Ejercicio_16 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese una lista de palabras separadas por espacios:");
        String input = sc.nextLine();

        String[] palabras = input.split(" ");
        ArrayList<String> palabrasConLetra = new ArrayList<>();

        System.out.println("Ingrese la letra para buscar:");
        String letra = sc.nextLine().toLowerCase();
        sc.close();
        for (String palabra : palabras) {
            if (palabra.toLowerCase().contains(letra)) {
                palabrasConLetra.add(palabra);
            }
        }

        System.out.println("La lista ingresada contiene " + palabrasConLetra.size() + " palabras que contienen la letra " + letra + ":");
        for (String palabra : palabrasConLetra) {
            System.out.println(palabra);
        }
    }
}
