import java.util.Scanner;

public class Ejercicio_20 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese los números separados por comas:");
        String numerosStr = sc.nextLine();

        System.out.println("Ingrese la cantidad mínima de divisores:");
        int cantDivisoresMinima = sc.nextInt();

        String[] numerosArr = numerosStr.split(",");
        int cantNumerosConDivisores = 0;
        sc.close();
        for (int i = 0; i < numerosArr.length; i++) {
            int num = Integer.parseInt(numerosArr[i]);
            int cantDivisores = contarDivisores(num);

            if (cantDivisores >= cantDivisoresMinima) {
                cantNumerosConDivisores++;
            }
        }

        System.out.println("Hay " + cantNumerosConDivisores + " números con al menos " + cantDivisoresMinima + " divisores.");
    }

    public static int contarDivisores(int num) {
        int cantDivisores = 0;

        for (int i = 1; i <= num; i++) {
            if (num % i == 0) {
                cantDivisores++;
            }
        }

        return cantDivisores;
    }
}
