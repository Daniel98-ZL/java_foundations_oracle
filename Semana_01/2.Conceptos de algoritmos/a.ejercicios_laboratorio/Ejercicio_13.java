import java.util.Scanner;

public class Ejercicio_13 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese una lista de números enteros separados por espacios:");
        String input = sc.nextLine();
        sc.close();
        String[] nums = input.split(" ");
        int fib = 0;

        for (String num : nums) {
            if (esNumeroDeFibonacci(Integer.parseInt(num))) {
                fib++;
            }
        }

        System.out.println("La lista ingresada contiene " + fib + " números de Fibonacci.");
    }

    public static boolean esNumeroDeFibonacci(int num) {
        if (num == 0 || num == 1) {
            return true;
        }
        int a = 0, b = 1, c = 0;
        while (c < num) {
            c = a + b;
            a = b;
            b = c;
        }
        return c == num;
    }
}
