import java.util.Scanner;
public class Ejercicio_03 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese un número entero: ");
        int numero1 = sc.nextInt();
        System.out.println("Ingrese otro número entero: ");
        int numero2 = sc.nextInt();
        System.out.println("Ingrese otro número entero");
        int numero3 = sc.nextInt();
        sc.close();
        
        if (numero1 > numero2 && numero1 > numero3) {
            System.out.println("El primer número ingresado es el mayor de los tres");
        } else if (numero2 > numero1 && numero2 > numero3) {
            System.out.println("El segundo número ingresado es el mayor de los tres");
        } else  {
            System.out.println("El tercer número ingresado es el mayor de los tres");
        }
    }
}
