import java.util.Scanner;

public class Ejercicio_04 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese los tres lados del triángulo: ");
        int a = sc.nextInt();
        int b = sc.nextInt();
        int c = sc.nextInt();
        sc.close();

        if (a + b > c && a + c > b && b + c > a) {
            System.out.println("Los lados ingresados pueden formar un triángulo válido.");
        } else {
            System.out.println("Los lados ingresados no pueden formar un triángulo válido.");
        }
    }
}
