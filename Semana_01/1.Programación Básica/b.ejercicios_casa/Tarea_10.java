import java.util.regex.Pattern;
import java.util.regex.Matcher;
public class Tarea_10 {
    public static void main(String[] args) {
        Pattern patron = Pattern.compile("\\d+");

        String texto = "Hola, mi número favorito es el 42";

        Matcher matcher = patron.matcher(texto);

        while (matcher.find()) {
            System.out.println("Número encontrado: " + matcher.group());
        }
    }
}
