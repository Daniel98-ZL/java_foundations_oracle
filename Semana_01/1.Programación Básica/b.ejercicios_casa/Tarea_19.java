import java.io.*;
public class Tarea_19 {
    public static void main(String[] args) {
        try {
            OutputStream outputStream = new FileOutputStream("hola.txt");
            
            outputStream.write('H');
            
            String mensaje = "hola usuario que novedades?";
            byte[] mensajeBytes = mensaje.getBytes();
            outputStream.write(mensajeBytes);
            
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
