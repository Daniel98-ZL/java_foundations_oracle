import java.util.Locale;
public class Tarea_17 {
    public static void main(String[] args) {
        Locale configuracionRegional = new Locale.Builder().setLanguage("es").setRegion("PE").build();

        String idioma = configuracionRegional.getLanguage();

        System.out.println("Idioma de la configuración regional: " + idioma);
    }
}
