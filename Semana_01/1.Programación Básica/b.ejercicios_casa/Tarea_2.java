import java.util.HashMap;
public class Tarea_2 {
    public static void main(String[] args) {
        HashMap<String, Integer> personas = new HashMap<>();
        personas.put("Juan", 24);
        personas.put("Luzvi", 23);
        personas.put("Luis", 29);
    
        for (String nombre : personas.keySet()) {
          System.out.println(nombre + " tiene " + personas.get(nombre) + " años.");
        }
      }
}
