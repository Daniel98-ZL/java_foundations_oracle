import java.util.Scanner;
public class Tarea_6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Ingresa tu nombre: ");
        
        String nombre = scanner.nextLine();
        scanner.close();
        System.out.println("Hola " + nombre + "!");
      }
}
