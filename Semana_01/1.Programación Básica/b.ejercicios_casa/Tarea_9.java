public class Tarea_9 {
    public static void main(String[] args) {
        int valorAbsoluto = Math.abs(-10);
        System.out.println("Valor absoluto de -10: " + valorAbsoluto);

        double raizCuadrada = Math.sqrt(25);
        System.out.println("Raíz cuadrada de 25: " + raizCuadrada);

        int maximo = Math.max(10, 20);
        System.out.println("Máximo entre 10 y 20: " + maximo);

        int minimo = Math.min(10, 20);
        System.out.println("Mínimo entre 10 y 20: " + minimo);

        double seno = Math.sin(Math.PI / 2);
        System.out.println("Seno de 90 grados: " + seno);

        double coseno = Math.cos(Math.PI / 2);
        System.out.println("Coseno de 90 grados: " + coseno);

        double pi = Math.PI;
        System.out.println("Valor de pi: " + pi);
    }
}
