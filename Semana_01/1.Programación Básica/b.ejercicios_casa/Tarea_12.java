import java.text.DateFormat;
import java.util.Date;
public class Tarea_12 {
    public static void main(String[] args) {

        DateFormat formatoFecha = DateFormat.getDateInstance();

        Date fecha = new Date();

        String fechaFormateada = formatoFecha.format(fecha);

        System.out.println("Fecha formateada: " + fechaFormateada);
    }
}
