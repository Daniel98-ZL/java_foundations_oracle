import java.io.*;
import java.net.*;
public class Tarea_20 {
    public static void main(String[] args) {
        try {
            Socket socket = new Socket("201.230.200.15", 27528);
            
            PrintWriter printWriter = new PrintWriter(socket.getOutputStream(), true);
            printWriter.println("Hola servidor!");
            
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            
            String respuesta = bufferedReader.readLine();
            System.out.println("Respuesta del servidor: " + respuesta);
            
            socket.close();
        } catch (UnknownHostException e) {
            System.err.println("Host desconocido");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
