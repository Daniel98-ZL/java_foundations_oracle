import java.util.TimeZone;
public class Tarea_16 {
    public static void main(String[] args) {
        TimeZone zonaHoraria = TimeZone.getTimeZone("America/Lima");

        String identificador = zonaHoraria.getID();

        System.out.println("Identificador de la zona horaria: " + identificador);
    }
}
