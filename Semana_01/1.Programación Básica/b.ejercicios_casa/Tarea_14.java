import java.math.BigDecimal;
public class Tarea_14 {
    public static void main(String[] args) {
        BigDecimal numero1 = new BigDecimal("1234.56");
        BigDecimal numero2 = new BigDecimal("7890.12");

        BigDecimal resultadoSuma = numero1.add(numero2);
        BigDecimal resultadoResta = numero1.subtract(numero2);
        BigDecimal resultadoMultiplicacion = numero1.multiply(numero2);

        System.out.println("Resultado de la suma: " + resultadoSuma);
        System.out.println("Resultado de la resta: " + resultadoResta);
        System.out.println("Resultado de la multiplicación: " + resultadoMultiplicacion);
    }
}
