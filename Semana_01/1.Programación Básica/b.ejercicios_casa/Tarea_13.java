import java.text.SimpleDateFormat;
import java.util.Date;
public class Tarea_13 {
    public static void main(String[] args) {
        
        SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        Date fecha = new Date();

        String fechaFormateada = formatoFecha.format(fecha);

        System.out.println("Fecha formateada: " + fechaFormateada);
    }
}
