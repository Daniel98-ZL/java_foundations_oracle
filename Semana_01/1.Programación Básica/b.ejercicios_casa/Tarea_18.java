import java.io.*;
public class Tarea_18 {
    public static void main(String[] args) {
        try {
            InputStream inputStream = new FileInputStream("C:/Users/zapan/Desktop/hola.txt");
            
            int byteLeido = inputStream.read();
            
            while (byteLeido != -1) {
                System.out.print((char) byteLeido); 
                
                byteLeido = inputStream.read();
            }
            
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
