import java.util.ArrayList;
public class Tarea_1 {
    public static void main(String[] args) {
        
        ArrayList<String> nombres = new ArrayList<String>();
        
        nombres.add("Juan");
        nombres.add("Daniel");
        nombres.add("Luis");
        nombres.add("Evelyn");
        nombres.add("Luzvi");
        
        System.out.println("Lista de nombres:");
        for (String nombre : nombres) {
            System.out.println(nombre);
        }
    }
}
