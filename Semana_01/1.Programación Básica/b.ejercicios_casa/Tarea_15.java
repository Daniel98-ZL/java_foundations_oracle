import java.math.BigInteger;
public class Tarea_15 {
    public static void main(String[] args) {
        BigInteger numero1 = new BigInteger("123456789");
        BigInteger numero2 = new BigInteger("987654321");

        BigInteger resultadoSuma = numero1.add(numero2);
        BigInteger resultadoResta = numero1.subtract(numero2);
        BigInteger resultadoMultiplicacion = numero1.multiply(numero2);
        BigInteger resultadoDivision = numero1.divide(numero2);

        System.out.println("Resultado de la suma: " + resultadoSuma);
        System.out.println("Resultado de la resta: " + resultadoResta);
        System.out.println("Resultado de la multiplicación: " + resultadoMultiplicacion);
        System.out.println("Resultado de la división: " + resultadoDivision);
    }
}
