import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class Tarea_11 {
    public static void main(String[] args) {
        Pattern patron = Pattern.compile("\\d+");

        String texto = "Hola, mi número favorito es el 42";

        Matcher matcher = patron.matcher(texto);

        if (matcher.find()) {
            System.out.println("Número encontrado: " + matcher.group());
        } else {
            System.out.println("No se encontró ningún número");
        }
    }
}
