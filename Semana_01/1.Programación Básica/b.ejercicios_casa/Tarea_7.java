import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
public class Tarea_7 {
    public static void main(String[] args) {
        BufferedReader br = null;
        try {
          // Creamos una variable BufferedReader para leer el archivo
          br = new BufferedReader(new FileReader("C:/Users/zapan/Desktop/hola.txt"));
    
          // Leemos el contenido del archivo
          String linea;
          while ((linea = br.readLine()) != null) {
            System.out.println(linea);
          }
        } catch (IOException e) {
          e.printStackTrace();
        } finally {
          try {
            if (br != null) {
              br.close();
            }
          } catch (IOException ex) {
            ex.printStackTrace();
          }
        }
      }
}
