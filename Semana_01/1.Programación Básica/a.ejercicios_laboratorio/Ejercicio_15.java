import java.util.Scanner;
public class Ejercicio_15 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese un número entero:");
        int numero = sc.nextInt();
        sc.close();
        
        boolean esPrimo = true;
        
        for (int i = 2; i <= numero / 2; i++) {
            if (numero % i == 0) {
                esPrimo = false;
                break;
            }
        }
        
        if (esPrimo) {
            System.out.println("El número ingresado es primo.");
        } else {
            System.out.println("El número ingresado no es primo.");
        }
    }
}
