import java.util.Scanner;
public class Ejercicio_11 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese el primer número entero:");
        int numero1 = sc.nextInt();
        System.out.println("Ingrese el segundo número entero:");
        int numero2 = sc.nextInt();
        sc.close();
        
        if (numero1 > numero2) {
            System.out.println("El primer número ingresado es mayor.");
        } else if (numero2 > numero1) {
            System.out.println("El segundo número ingresado es mayor.");
        } else {
            System.out.println("Los dos números ingresados son iguales.");
        }
    }
}
