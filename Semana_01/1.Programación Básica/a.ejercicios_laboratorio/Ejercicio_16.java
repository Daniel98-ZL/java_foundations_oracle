import java.util.Scanner;
public class Ejercicio_16 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese su peso en kilogramos:");
        double peso = sc.nextDouble();
        
        System.out.println("Ingrese su altura en metros:");
        double altura = sc.nextDouble();
        sc.close();
        
        double imc = peso / (altura * altura);
        
        System.out.println("Su índice de masa corporal es: " + imc);
    }
}
