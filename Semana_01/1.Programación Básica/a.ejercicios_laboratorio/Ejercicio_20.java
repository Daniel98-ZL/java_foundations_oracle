import java.util.Scanner;
public class Ejercicio_20 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int num1, num2, mcd, mcm;

        System.out.print("Ingrese el primer número entero: ");
        num1 = sc.nextInt();
        System.out.print("Ingrese el segundo número entero: ");
        num2 = sc.nextInt();
        sc.close();
        mcd = calcularMCD(num1, num2);
        mcm = calcularMCM(num1, num2, mcd);

        System.out.println("El Máximo Común Divisor (MCD) de " + num1 + " y " + num2 + " es " + mcd);
        System.out.println("El Mínimo Común Múltiplo (MCM) de " + num1 + " y " + num2 + " es " + mcm);
    }

    public static int calcularMCD(int num1, int num2) {
        int resto;

        do {
            resto = num1 % num2;
            num1 = num2;
            num2 = resto;
        } while (resto != 0);

        return num1;
    }

    public static int calcularMCM(int num1, int num2, int mcd) {
        return (num1 * num2) / mcd;
    }
}
