import java.util.Scanner;

public class Ejercicio_3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese un número entero");
        int Numero1 = sc.nextInt();
        System.out.println("Ingrese otro número entero");
        int Numero2 = sc.nextInt();
        sc.close();
        int suma = Numero1 + Numero2;

        System.out.println("El resultado de la suma de sus números es: " + suma);

    }
}
