import java.util.Scanner;
public class Ejercicio_13 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese una cadena de texto:");
        String cadena = sc.nextLine();
        sc.close();
        
        System.out.println("La cadena ingresada es: " + cadena);
    }
}
