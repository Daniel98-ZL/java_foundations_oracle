import java.util.Scanner;
public class Ejercicio_8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese la longitud del lado 1 del triángulo:");
        double lado1 = sc.nextDouble();
        System.out.println("Ingrese la longitud del lado 2 del triángulo:");
        double lado2 = sc.nextDouble();
        System.out.println("Ingrese la longitud del lado 3 del triángulo:");
        double lado3 = sc.nextDouble();
        sc.close();
        
        if (lado1 == lado2 && lado1 == lado3) {
            System.out.println("El triángulo es equilátero.");
        } else if (lado1 == lado2 || lado1 == lado3 || lado2 == lado3) {
            System.out.println("El triángulo es isósceles.");
        } else {
            System.out.println("El triángulo es escaleno.");
        }
    }
}
