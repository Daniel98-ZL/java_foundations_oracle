import java.util.Scanner;

public class Ejercicio_4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese un número entero");
        int Numero1 = sc.nextInt();
        System.out.println("Ingrese otro número entero");
        int Numero2 =sc.nextInt();
        sc.close();
        int multiplicación = Numero1 * Numero2;

        System.out.println("El resultado de la multiplicación de sus números es: " + multiplicación);

    }
}
