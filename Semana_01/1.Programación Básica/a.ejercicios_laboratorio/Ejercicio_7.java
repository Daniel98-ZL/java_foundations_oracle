import java.util.Scanner;

public class Ejercicio_7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese la base de su triangulo");
        float Base = sc.nextFloat();
        System.out.println("Ingrese la altura de su triangulo");
        float Altura = sc.nextFloat();
        sc.close();
        float area = (Base * Altura) / 2;

        System.out.println("El área de su triangulo es: " + area);
    }
}
