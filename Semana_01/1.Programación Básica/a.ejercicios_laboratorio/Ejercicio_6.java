import java.util.Scanner;

public class Ejercicio_6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese un número entero");
        int Numero1 = sc.nextInt();
        System.out.println("Ingrese otro número entero");
        int Numero2 = sc.nextInt();
        sc.close();
        int cociente = Numero1 / Numero2;
        int residuo = Numero1 % Numero2;

        System.out.println("El resultado de la division, cociente " + cociente + " y el resto es " + residuo);

    }
}
