import java.util.Scanner;
public class Ejercicio_19 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese un número entero:");
        int numero = sc.nextInt();
        sc.close();
        
        int suma = 0;
        
        for (int i = 1; i <= numero; i++) {
            suma += i;
        }
        
        System.out.println("La suma de los números enteros desde 1 hasta " + numero + " es " + suma);
    }
}
