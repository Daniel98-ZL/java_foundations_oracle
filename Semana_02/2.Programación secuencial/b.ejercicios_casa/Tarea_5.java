import java.util.Arrays;

public class Tarea_5 {
    public static void main(String[] args) {
        int[] arr = {1, 2, 4, 3, 7, 5, 6, 9, 8};
        int[] dp = new int[arr.length];
        Arrays.fill(dp, 1);
        int maxIndex = 0;
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > arr[i-1]) {
                dp[i] = dp[i-1] + 1;
            }
            if (dp[i] > dp[maxIndex]) {
                maxIndex = i;
            }
        }
        int maxLength = dp[maxIndex];
        int[] subarray = new int[maxLength];
        subarray[maxLength-1] = arr[maxIndex];
        for (int i = maxIndex-1, j = maxLength-2; i >= 0 && j >= 0; i--) {
            if (arr[i] < subarray[j+1]) {
                break;
            }
            subarray[j] = arr[i];
            j--;
        }
        System.out.println("Subarreglo máximo creciente: " + Arrays.toString(subarray));
    }

}
