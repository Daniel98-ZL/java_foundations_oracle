public class Tarea_11 {
    public static void main(String[] args) {
        int[] arregloEnteros = { 5, 10, 15, 20, 25 };
        int suma = 0;
        for (int i = 0; i < arregloEnteros.length; i++) {
            suma += arregloEnteros[i];
        }
        double media = (double) suma / arregloEnteros.length;
        System.out.println("La media es: " + media);
    }
}
