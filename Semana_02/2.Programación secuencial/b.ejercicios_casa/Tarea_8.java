import java.util.Scanner;

public class Tarea_8 {
    public static boolean esPrimo(int n) {
        if (n < 2) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(n); i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese la longitud del arreglo: ");
        int n = sc.nextInt();
        int[] arreglo = new int[n];
        System.out.println("Ingrese los elementos del arreglo: ");
        for (int i = 0; i < n; i++) {
            arreglo[i] = sc.nextInt();
        }
        int contadorParejas = 0;
        for (int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                int suma = arreglo[i] + arreglo[j];
                if (esPrimo(suma)) {
                    contadorParejas++;
                }
            }
        }
        sc.close();
        System.out.println("El número de parejas cuya suma es un número primo es: " + contadorParejas);
    }
}
