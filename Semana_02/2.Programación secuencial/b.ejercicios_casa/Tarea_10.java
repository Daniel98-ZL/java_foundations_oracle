import java.util.HashMap;
import java.util.Map;

public class Tarea_10 {
    public static int moda(int[] arr) {
        Map<Integer, Integer> freq = new HashMap<>();
        int maxFreq = 0, moda = -1;

        for (int num : arr) {
            int newFreq = freq.getOrDefault(num, 0) + 1;
            freq.put(num, newFreq);

            if (newFreq > maxFreq) {
                maxFreq = newFreq;
                moda = num;
            }
        }

        return moda;
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 2, 3, 3, 4, 5, 5, 5, 6, 6, 6, 6};
        int moda = moda(arr);
        System.out.println("La moda del arreglo es: " + moda);
    }
}
