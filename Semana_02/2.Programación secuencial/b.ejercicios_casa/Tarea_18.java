public class Tarea_18 {
    public static void main(String[] args) {
        int[] arr1 = {1, 2, 3};
        int[] arr2 = {4, 5, 6};
        int productoPunto = 0;
        for (int i = 0; i < arr1.length; i++) {
            productoPunto += arr1[i] * arr2[i];
        }
        System.out.println("El producto punto es: " + productoPunto);
    }
}
