public class Tarea_13 {
    public static void main(String[] args) {
        int[] arregloEnteros = { 5, 10, 15, 20, 25 };
        int contadorImpares = 0;
        for (int i = 0; i < arregloEnteros.length; i++) {
            if (arregloEnteros[i] % 2 != 0) {
                contadorImpares++;
            }
        }
        System.out.println("Hay " + contadorImpares + " números impares en el arreglo.");
    }
}
