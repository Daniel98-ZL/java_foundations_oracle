public class Tarea_1 {
    public static void main(String[] args) {
        int[][] matriz = new int[7][7];
        int[][] resultado = new int[7][7];
        
        
        int contador = 1;
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                matriz[i][j] = contador;
                contador++;
            }
        }
        
        
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                resultado[j][i] = matriz[i][j];
            }
        }
        
        
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7 / 2; j++) {
                int temp = resultado[i][j];
                resultado[i][j] = resultado[i][6 - j];
                resultado[i][6 - j] = temp;
            }
        }
        
        
        System.out.println("Matriz original:");
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                System.out.print(matriz[i][j] + "\t");
            }
            System.out.println();
        }
        
        
        System.out.println("\nMatriz rotada 90 grados en sentido horario:");
        for (int i = 0; i < 7; i++) {
            for (int j = 0; j < 7; j++) {
                System.out.print(resultado[i][j] + "\t");
            }
            System.out.println();
        }
    }
}
