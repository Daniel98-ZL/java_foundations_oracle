import java.util.Random;

public class Tarea_7 {
    public static int findKthLargest(int[] nums, int k) {
        int n = nums.length;
        int left = 0, right = n - 1;
        Random rand = new Random();

        while (left <= right) {
            
            int pivotIndex = rand.nextInt(right - left + 1) + left;
            int newPivotIndex = partition(nums, left, right, pivotIndex);

            
            if (newPivotIndex == n - k) {
                return nums[newPivotIndex];
            } else if (newPivotIndex < n - k) {
                
                left = newPivotIndex + 1;
            } else {
                
                right = newPivotIndex - 1;
            }
        }

        return -1; 
    }

    private static int partition(int[] nums, int left, int right, int pivotIndex) {
        int pivotValue = nums[pivotIndex];
        int newPivotIndex = left;

        swap(nums, pivotIndex, right);

        for (int i = left; i < right; i++) {
            if (nums[i] < pivotValue) {
                swap(nums, i, newPivotIndex);
                newPivotIndex++;
            }
        }

        swap(nums, right, newPivotIndex);

        return newPivotIndex;
    }

    private static void swap(int[] nums, int i, int j) {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }

    public static void main(String[] args) {
        int[] nums = {3, 2, 1, 5, 6, 4};
        int k = 2;
        int kthLargest = findKthLargest(nums, k);
        System.out.println(k + "-ésimo elemento más grande: " + kthLargest);
    }
}
