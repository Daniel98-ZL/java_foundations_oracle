import java.util.Arrays;
import java.util.Comparator;

public class Tarea_6 {
    public static void main(String[] args) {
        String[] arreglo = {"hola", "adios", "buenos dias", "buenas noches", "como estas"};

        Arrays.sort(arreglo, Comparator.reverseOrder());

        System.out.println(Arrays.toString(arreglo));
    }
}
