import java.util.HashMap;
import java.util.Map;
public class Tarea_16 {
    public static void main(String[] args) {
        int[] arr = {2, 3, 4, 2, 3, 2, 4, 5, 2, 4};
        HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
        for (int i = 0; i < arr.length; i++) {
            if (map.containsKey(arr[i])) {
                map.put(arr[i], map.get(arr[i]) + 1);
            } else {
                map.put(arr[i], 1);
            }
        }
        int max = 0;
        int numMasRepetido = 0;
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            int value = entry.getValue();
            if (value > max) {
                max = value;
                numMasRepetido = entry.getKey();
            }
        }
        System.out.println("El número que se repite más veces es " + numMasRepetido + ", que aparece " + max + " veces.");
    }
}
