import java.util.Arrays;
public class Tarea_17 {
    public static void main(String[] args) {
        int[] arr = {2, 3, 4, 2, 3, 2, 4, 5, 2, 4};
        int longitud = arr.length;
        int longitudParte = longitud / 2;
        int[] primeraParte = new int[longitudParte];
        int[] segundaParte = new int[longitudParte];
        System.arraycopy(arr, 0, primeraParte, 0, longitudParte);
        System.arraycopy(arr, longitudParte, segundaParte, 0, longitudParte);
        System.out.println("Primera parte: " + Arrays.toString(primeraParte));
        System.out.println("Segunda parte: " + Arrays.toString(segundaParte));
    }
}
