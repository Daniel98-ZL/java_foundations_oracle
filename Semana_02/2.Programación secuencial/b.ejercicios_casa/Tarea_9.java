import java.util.Arrays;

public class Tarea_9 {
    public static double encontrarMediana(int[] arreglo) {
        
        Arrays.sort(arreglo);
        
        int n = arreglo.length;
        
        boolean par = (n % 2 == 0);
        
        
        if (par) {
            int i = n/2 - 1;
            int j = n/2;
            return (double) (arreglo[i] + arreglo[j]) / 2.0;
        }
        
        else {
            int i = n/2;
            return (double) arreglo[i];
        }
    }
    
    public static void main(String[] args) {
        int[] arreglo = {10, 5, 2, 6, 8, 4, 3, 7, 1, 9};
        double mediana = encontrarMediana(arreglo);
        System.out.println("La mediana del arreglo es: " + mediana);
    }
}