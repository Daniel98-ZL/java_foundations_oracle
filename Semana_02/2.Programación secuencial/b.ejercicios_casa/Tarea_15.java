public class Tarea_15 {
    public static void main(String[] args) {
        int[] arregloEnteros = { 2, 5, 10, 13, 14, 16, 18 };
        int contadorPares = 0;
        for (int i = 0; i < arregloEnteros.length; i++) {
            if (arregloEnteros[i] % 2 == 0) {
                contadorPares++;
            }
        }
        System.out.println("Hay " + contadorPares + " números pares en el arreglo.");
    }
}
