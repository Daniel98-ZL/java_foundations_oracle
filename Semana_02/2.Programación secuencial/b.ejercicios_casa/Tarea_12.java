public class Tarea_12 {
    public static void main(String[] args) {
        int[] arregloEnteros = { 5, 10, 15, 20, 25 };
        int maximo = arregloEnteros[0];
        for (int i = 1; i < arregloEnteros.length; i++) {
            if (arregloEnteros[i] > maximo) {
                maximo = arregloEnteros[i];
            }
        }
        System.out.println("El número más grande en el arreglo es: " + maximo);
    }
}
