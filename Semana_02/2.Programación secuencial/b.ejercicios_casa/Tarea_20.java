import java.util.Arrays;

public class Tarea_20 {
    public static void main(String[] args) {
        int[] codigosAscii = new int[128];
        for (int i = 0; i < 128; i++) {
            codigosAscii[i] = i;
        }
        System.out.println(Arrays.toString(codigosAscii));
    }
}
