public class Tarea_14 {
    public static void main(String[] args) {
        int[] arregloEnteros = { 5, 10, 10, 15, 20, 25, 25 };
        int numeroBuscado = 10;
        int contador = 0;
        int izquierda = 0;
        int derecha = arregloEnteros.length - 1;
        while (izquierda <= derecha) {
            int medio = (izquierda + derecha) / 2;
            if (arregloEnteros[medio] == numeroBuscado) {
                contador++;
                int i = medio - 1;
                while (i >= izquierda && arregloEnteros[i] == numeroBuscado) {
                    contador++;
                    i--;
                }
                i = medio + 1;
                while (i <= derecha && arregloEnteros[i] == numeroBuscado) {
                    contador++;
                    i++;
                }
                break;
            } else if (arregloEnteros[medio] < numeroBuscado) {
                izquierda = medio + 1;
            } else {
                derecha = medio - 1;
            }
        }
        System.out.println("El número " + numeroBuscado + " aparece " + contador );
    }
}
