import java.util.Random;

public class Tarea_2 {
    public static void main(String[] args) {
        int[][] matriz = new int[7][7];
        Random random = new Random();
        for (int i = 0; i < matriz.length; i++) {
            for (int j = 0; j < matriz[i].length; j++) {
                matriz[i][j] = random.nextInt(100) + 1;
                System.out.print(matriz[i][j] + " ");
            }
            System.out.println();
        }
    }
}
