import java.util.Scanner;

public class Ejercicio_5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese un número: ");
        int numero = sc.nextInt();
        String numeroRomano = convertirARomano(numero);
        System.out.println(numero + " en números romanos es: " + numeroRomano);
        sc.close();
    }
    
    public static String convertirARomano(int numero) {
        if (numero < 1 || numero > 3999) {
            return "El número debe estar entre 1 y 3999";
        }
        
        String[] unidadesRomanas = {"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"};
        String[] decenasRomanas = {"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"};
        String[] centenasRomanas = {"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"};
        String[] unidadesDeMilRomanas = {"", "M", "MM", "MMM"};
        
        String unidad = unidadesRomanas[numero % 10];
        String decena = decenasRomanas[(numero / 10) % 10];
        String centena = centenasRomanas[(numero / 100) % 10];
        String unidadDeMil = unidadesDeMilRomanas[numero / 1000];
        
        return unidadDeMil + centena + decena + unidad;
    }
}