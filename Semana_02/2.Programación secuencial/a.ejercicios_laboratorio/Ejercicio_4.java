public class Ejercicio_4 {
    public static int encontrarMaximo(int[] arreglo) {
        int maximo = arreglo[0]; 
        
        for (int i = 1; i < arreglo.length; i++) {
            if (arreglo[i] > maximo) {
                maximo = arreglo[i];
            }
        }
        
        return maximo;
    }
    public static void main(String[] args) {
        int[] arreglo = {11, 50, 30, 100, 2000};
        int maximo = encontrarMaximo(arreglo);
        System.out.println("El elemento máximo del arreglo es: " + maximo);
    }
}