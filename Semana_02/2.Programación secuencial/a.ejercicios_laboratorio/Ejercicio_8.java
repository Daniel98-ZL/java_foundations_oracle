public class Ejercicio_8 {
    public static double calcularVarianza(int[] arr) {
        double media = 0;
        for(int i = 0; i < arr.length; i++) {
            media += arr[i];
        }
        media /= arr.length;
    
        double suma = 0;
        for(int i = 0; i < arr.length; i++) {
            suma += Math.pow(arr[i] - media, 2);
        }
        return suma / arr.length;
    }
    public static void main(String[] args) {
        int[] arr = {2, 4, 6, 8, 10};
        double varianza = calcularVarianza(arr);
        System.out.println("La varianza es: " + varianza);
    }
}
