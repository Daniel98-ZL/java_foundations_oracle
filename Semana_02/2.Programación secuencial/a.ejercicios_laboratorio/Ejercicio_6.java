import java.util.Scanner;

public class Ejercicio_6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int numero;
        
        do {
            System.out.print("Ingrese un número entre 1000 y 7000: ");
            numero = sc.nextInt();
        } while (numero < 1000 || numero > 7000);
        
        int[] digitos = obtenerDigitos(numero);
        sc.close();
        System.out.println("Los dígitos del número " + numero + " son:");
        for (int i = 0; i < digitos.length; i++) {
            System.out.println("Dígito " + (i + 1) + ": " + digitos[i]);
        }
    }
    
    public static int[] obtenerDigitos(int numero) {
        int[] digitos = new int[4];
        digitos[0] = numero / 1000; 
        digitos[1] = (numero / 100) % 10; 
        digitos[2] = (numero / 10) % 10; 
        digitos[3] = numero % 10; 
        
        return digitos;
    }
}