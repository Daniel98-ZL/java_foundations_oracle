//Crear un método que calcule la suma de los elementos de un arreglo de enteros.
public class Ejercicio_1 {
    public static int sumarElementos(int[] arreglo) {
        int suma = 0;
        for (int i = 0; i < arreglo.length; i++) {
            suma += arreglo[i];
        }
        return suma;
    }

    public static void main(String[] args) {
        int[] miArreglo = { 12, 256, 323, 456, 522 };
        int suma = sumarElementos(miArreglo);
        System.out.println("La suma de los elementos del arreglo es: " + suma);
    }
}
