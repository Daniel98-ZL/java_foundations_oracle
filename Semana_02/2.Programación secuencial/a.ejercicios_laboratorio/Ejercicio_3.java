import java.util.Scanner;

public class Ejercicio_3 {
    public static void main(String[] args) {
        // Definir el arreglo bidimensional de estudiantes
        String[][] estudiantes = new String[5][2];

        // Pedir los datos de los estudiantes
        pedirDatos(estudiantes);

        // Calcular la edad promedio de los estudiantes
        double promedio = calcularPromedio(edadEstudiantes(estudiantes));

        // Mostrar los datos y el promedio de edad de los estudiantes
        mostrarDatos(estudiantes, promedio);
    }

    // Método para pedir los datos de los estudiantes
    public static void pedirDatos(String[][] estudiantes) {
        Scanner sc = new Scanner(System.in);

        for (int i = 0; i < estudiantes.length; i++) {
            System.out.println("Ingrese el nombre del estudiante #" + (i + 1) + ": ");
            estudiantes[i][0] = sc.nextLine();
            System.out.println("Ingrese la edad del estudiante #" + (i + 1) + ": ");
            estudiantes[i][1] = sc.nextLine();
        }sc.close();
    }
    
    // Método para calcular la edad promedio de los estudiantes
    public static double calcularPromedio(int[] edades) {
        int suma = 0;
        for (int i = 0; i < edades.length; i++) {
            suma += edades[i];
        }
        double promedio = (double) suma / edades.length;
        return promedio;
    }
    // Método para obtener las edades de los estudiantes
    public static int[] edadEstudiantes(String[][] estudiantes) {
        int[] edades = new int[estudiantes.length];
        for (int i = 0; i < estudiantes.length; i++) {
            edades[i] = Integer.parseInt(estudiantes[i][1]);
        }
        return edades;
    }
    
    // Método para mostrar los datos y el promedio de edad de los estudiantes
    public static void mostrarDatos(String[][] estudiantes, double promedio) {
        System.out.println("Datos de los estudiantes de Ing. del Software de SENATI: ");
        for (int i = 0; i < estudiantes.length; i++) {
            System.out.println("Nombre: " + estudiantes[i][0] + ", Edad: " + estudiantes[i][1]);
        }
        System.out.println("La edad promedio de los estudiantes es: " + promedio);
    }
}