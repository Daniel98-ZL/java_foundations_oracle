// Crear un método que calcule el producto de los elementos de un arreglo de enteros.
public class Ejercicio_2 {
    public static int multiplicarArreglo(int[] arreglo) {
        int producto = 1;
        for (int i = 0; i < arreglo.length; i++) {
            producto *= arreglo[i];
        }
        return producto;
    }
    public static void main(String[] args) {
        int[] arreglo = {6,10};
        int resultado = multiplicarArreglo(arreglo);
        System.out.println("El producto de los elementos es: " + resultado);
    }
}
