//package Semana_02.a.ejercicios_laboratorio;
//Desarrollar un programa que sume los "N" números impares.
import java.util.Scanner;
public class Ejercicio_2 {
    public static void main(String[] args) {
        Scanner tecla = new Scanner (System.in);
        int numero ,inicial = 1 ,suma = 0;

        System.out.print("Introduce un numero: ");
        numero = tecla.nextInt();
        tecla.close();
        
        while(inicial <= numero)
        {
            if(inicial % 2 !=0)
            {
                suma = suma + inicial;
            }
            else
            {
                System.out.println("es par este numero " + inicial + " por lo tanto no ingresa a la suma!!!!!");
            }
            inicial++;
        }

        System.out.println("\n La suma de los numeros impares es: "+ suma);
    }
}
