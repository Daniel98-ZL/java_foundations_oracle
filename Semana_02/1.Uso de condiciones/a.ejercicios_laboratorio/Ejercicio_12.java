import java.util.Scanner;

public class Ejercicio_12 {
    public static void main(String[] args) {
      
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduce el número de valores: ");
        int n = sc.nextInt();
        
        int[] valores = new int[n];
        for (int i = 0; i < n; i++) {
           System.out.print("Introduce el valor " + (i+1) + ": ");
           valores[i] = sc.nextInt();
        }
        
        int mcm = valores[0];
        sc.close();
        for (int i = 1; i < n; i++) {
           mcm = mcm * valores[i] / mcd(mcm, valores[i]);
        }
        
        System.out.println("El mínimo común múltiplo de los " + n + " valores es: " + mcm);
        
     }
     
     public static int mcd(int a, int b) {
        int resto;
        while (b != 0) {
           resto = a % b;
           a = b;
           b = resto;
        }
        return a;
    }
}
