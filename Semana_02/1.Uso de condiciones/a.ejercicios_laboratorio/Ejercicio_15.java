import java.util.Scanner;

public class Ejercicio_15 {
    public static void main(String[] args) {
      
        // Pedir al usuario que introduzca el número límite
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduce el número límite: ");
        int limite = sc.nextInt();
        
        // Inicializar los primeros dos números de la secuencia de Lucas
        int a = 2;
        int b = 1;
        
        // Imprimir los números de la secuencia de Lucas hasta el límite
        System.out.println("Secuencia de Lucas:");
        System.out.print(a + " ");
        sc.close();
        while (b <= limite) {
           System.out.print(b + " ");
           int c = a + b;
           a = b;
           b = c;
        }
        
     }
}
