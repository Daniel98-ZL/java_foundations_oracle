import java.util.Scanner;

public class Ejercicio_14 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingresa un número para calcular su factorial: ");
        int num = sc.nextInt();
        
        int factorial = 1;
        int i = 1;
        while (i <= num) {
            factorial *= i;
            i++;
        }
        
        System.out.println("El factorial de " + num + " es " + factorial);
        sc.close();
    }

}
