import java.util.Scanner;
public class Ejercicio_4 {
public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese el número de números perfectos a sumar: ");
        int n = sc.nextInt();

        int suma = 0;
        int contador = 0;
        int numero = 1;
        sc.close();
        while (contador < n) {
            if (esPerfecto(numero)) {
                suma += numero;
                contador++;
            }
            numero++;
        }

        System.out.println("La suma de los primeros " + n + " números perfectos es: " + suma);
    }
    public static boolean esPerfecto(int n) {
        int suma = 0;
        for (int i = 1; i < n; i++) {
            if (n % i == 0) {
                suma += i;
            }
        }
        return suma == n;
    }
}

