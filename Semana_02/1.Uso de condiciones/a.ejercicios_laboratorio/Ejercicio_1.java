import java.util.Scanner;

//package Semana_02.a.ejercicios_laboratorio;

public class Ejercicio_1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Introduce una palabra: ");
        String entrada = scanner.nextLine();
        scanner.close();
        int i = 0;
        while (i < entrada.length()) {
            System.out.println("letra: "+ entrada.substring(i, i + 1));
            i++;
        }
    }
}
