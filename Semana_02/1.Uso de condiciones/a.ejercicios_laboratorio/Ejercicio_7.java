import java.util.Scanner;
public class Ejercicio_7 {
    public static void main(String[] args) {
      
        
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduce el valor de N: ");
        int n = sc.nextInt();
        
        
        int suma = 0;
        int i = 1;
        sc.close();
        
        do {
           if (i % 2 == 0) {
              suma += i;
           }
           i++;
        } while (i < 2 + n);
        
        System.out.println("La suma de los " + n + " primeros números pares es: " + suma);
        
    }
}

