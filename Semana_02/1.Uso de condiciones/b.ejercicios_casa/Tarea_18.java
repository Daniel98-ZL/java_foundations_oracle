import java.util.Scanner;

public class Tarea_18 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int limiteInferior, limiteSuperior;
        do {
            System.out.print("Ingrese el límite inferior del intervalo: ");
            limiteInferior = sc.nextInt();
            System.out.print("Ingrese el límite superior del intervalo: ");
            limiteSuperior = sc.nextInt();
            if (limiteInferior > limiteSuperior) {
                System.out.println("El límite inferior debe ser menor o igual al límite superior. Intente de nuevo.");
            }
        } while (limiteInferior > limiteSuperior);

        int numero, suma = 0, fueraIntervalo = 0;
        boolean limiteInferiorEncontrado = false, limiteSuperiorEncontrado = false;

        System.out.println("Ingrese números (0 para terminar):");
        do {
            numero = sc.nextInt();
            if (numero != 0) {
                if (numero > limiteInferior && numero < limiteSuperior) {
                    suma += numero;
                } else {
                    fueraIntervalo++;
                }
                if (numero == limiteInferior) {
                    limiteInferiorEncontrado = true;
                }
                if (numero == limiteSuperior) {
                    limiteSuperiorEncontrado = true;
                }
            }
        } while (numero != 0);

        System.out.println("La suma de los números dentro del intervalo es: " + suma);
        System.out.println("Hay " + fueraIntervalo + " números fuera del intervalo.");
        if (limiteInferiorEncontrado) {
            System.out.println("Se ha introducido el límite inferior del intervalo.");
        }
        if (limiteSuperiorEncontrado) {
            System.out.println("Se ha introducido el límite superior del intervalo.");
        }sc.close();
    }
}
