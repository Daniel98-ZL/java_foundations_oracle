import java.util.Scanner;

public class Tarea_14 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese un número entero positivo: ");
        int n = sc.nextInt();
        sc.close();
        
        if (n <= 0) {
            System.out.println("El número ingresado debe ser positivo.");
        } else {
            String impares = "";
            for (int i = 1; i <= n; i++) {
                if (i % 2 != 0) {
                    impares += i + "; ";
                }
            }
            System.out.println("Los números impares hasta " + n + " son: " + impares);
        }
    }
}
