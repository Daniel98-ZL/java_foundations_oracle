import java.util.Scanner;

public class Tarea_20 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese la cantidad de números perfectos que desea calcular: ");
        int n = sc.nextInt();
        
        int num = 2;
        int count = 0;
        int sum = 0;
        while (count < n) {
            if (esPerfecto(num)) {
                count++;
                sum += num;
                System.out.println("Número perfecto encontrado: " + num);
            }
            num++;
        }
        sc.close();
        System.out.println("La suma de los primeros " + n + " números perfectos es: " + sum);
    }
    
    public static boolean esPerfecto(int num) {
        int sum = 0;
        for (int i = 1; i <= num/2; i++) {
            if (num % i == 0) {
                sum += i;
            }
        }
        return sum == num;
    }
}
