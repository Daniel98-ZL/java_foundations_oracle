import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;
public class Tarea_10 {
    public static void main(String[] args) {
        
        int[] datos = leerDatos("datos.txt");

        
        double promedio = calcularPromedio(datos);
        System.out.println("El promedio es: " + promedio);

        
        int moda = calcularModa(datos);
        System.out.println("La moda es: " + moda);

        
        double desviacion = calcularDesviacion(datos, promedio);
        System.out.println("La desviación estándar es: " + desviacion);
    }

    private static int[] leerDatos(String archivo) {
        try {
            File file = new File(archivo);
            Scanner sc = new Scanner(file);
            int[] datos = new int[27];
            int i = 0;
            while (sc.hasNextInt()) {
                datos[i] = sc.nextInt();
                i++;
            }
            sc.close();
            return datos;
        } catch (FileNotFoundException e) {
            System.out.println("Archivo no encontrado");
            return null;
        }
    }

    private static double calcularPromedio(int[] datos) {
        int suma = 0;
        for (int dato : datos) {
            suma += dato;
        }
        return (double) suma / datos.length;
    }

    private static int calcularModa(int[] datos) {
        Arrays.sort(datos);
        int moda = datos[0];
        int maximoFrecuencia = 1;
        int frecuenciaActual = 1;
        for (int i = 1; i < datos.length; i++) {
            if (datos[i] == datos[i - 1]) {
                frecuenciaActual++;
            } else {
                if (frecuenciaActual > maximoFrecuencia) {
                    maximoFrecuencia = frecuenciaActual;
                    moda = datos[i - 1];
                }
                frecuenciaActual = 1;
            }
        }
        if (frecuenciaActual > maximoFrecuencia) {
            moda = datos[datos.length - 1];
        }
        return moda;
    }

    private static double calcularDesviacion(int[] datos, double promedio) {
        double suma = 0;
        for (int dato : datos) {
            suma += Math.pow(dato - promedio, 2);
        }
        double varianza = suma / datos.length;
        return Math.sqrt(varianza);
    }
}
