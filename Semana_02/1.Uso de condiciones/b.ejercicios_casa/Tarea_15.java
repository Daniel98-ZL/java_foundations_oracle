public class Tarea_15 {
    public static void main(String[] args) {
        double capital = 5000;
        double tasa = 0.016;
        int tiempo = 18;

        for (int mes = 1; mes <= tiempo; mes++) {
            double saldo = capital * Math.pow(1 + tasa, mes);
            System.out.printf("Mes %d: saldo = %.2f soles\n", mes, saldo);
        }
    }
}
