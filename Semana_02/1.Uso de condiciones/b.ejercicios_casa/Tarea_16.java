import java.util.Scanner;

public class Tarea_16 {
    public class TrianguloRectangulo {
        public static void main(String[] args) {
            Scanner sc = new Scanner(System.in);
            System.out.print("Ingrese un número entero: ");
            int num = sc.nextInt();
            int filas = num/2 + 1;
            sc.close();
            for (int i = 1; i <= filas; i++) {
                for (int j = 2*i-1; j >= 1; j -= 2) {
                    System.out.print(j + " ");
                }
                System.out.println();
            }
        }
    }
}
