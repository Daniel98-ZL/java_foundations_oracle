public class Tarea_3 {
    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Por favor, introduzca un número como argumento.");
            return;
        }

        int num = Integer.parseInt(args[0]);
        int factorial = 1;

        for (int i = 1; i <= num; i++) {
            factorial *= i;
        }

        System.out.println(num + "! = " + factorial);
    }
}
