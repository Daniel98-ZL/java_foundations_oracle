import java.util.Scanner;

//package Semana_02.b.ejercicios_casa;

public class Tarea_1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Introduce una palabra: ");
        String palabra = scanner.nextLine();
        scanner.close();
        int i = 0;
        while (i < palabra.length()) {
            System.out.println("Letra " + (i + 1) + ": " + palabra.substring(i, i + 1));
            i++;
        }
    }
}
