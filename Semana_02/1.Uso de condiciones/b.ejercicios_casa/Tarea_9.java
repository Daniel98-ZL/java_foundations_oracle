import java.util.Scanner;

public class Tarea_9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese el primer término: ");
        double a = sc.nextDouble();
        System.out.print("Ingrese la razón común: ");
        double r = sc.nextDouble();
        System.out.print("Ingrese el número de términos: ");
        int n = sc.nextInt();
        double suma = a * (1 - Math.pow(r, n)) / (1 - r);
        System.out.println("La suma de los primeros " + n + " términos es: " + suma);
        sc.close();
    }
}
