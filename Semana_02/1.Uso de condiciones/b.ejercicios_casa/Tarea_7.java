import java.util.Scanner;

public class Tarea_7 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.print("Ingrese el valor de n: ");
        int n = sc.nextInt();
        
        int[] ulam = new int[n];
        ulam[0] = 1;
        ulam[1] = 2;
        
        for (int i = 2; i < n; i++) {
            int sum = Integer.MAX_VALUE;
            for (int j = 0; j < i; j++) {
                for (int k = j + 1; k < i; k++) {
                    if (ulam[j] + ulam[k] == ulam[i - 1] && ulam[j] != ulam[k]) {
                        sum = Math.min(sum, ulam[j] + ulam[k]);
                    }
                }
            }
            ulam[i] = sum;
        }
        sc.close();
        System.out.println("Los primeros " + n + " términos de la sucesión de Ulam son:");
        for (int i = 0; i < n; i++) {
            System.out.print(ulam[i] + " ");
        }
    }
}
