import java.util.Scanner;

public class Tarea_13 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese los segundos a contar: ");
        int segundos = scanner.nextInt();
        scanner.close();
        
        for(int i=segundos; i>=0; i--) {
            int minutos = i/60;
            int segundosRestantes = i%60;
            System.out.printf("%02d:%02d\n", minutos, segundosRestantes);
            try {
                Thread.sleep(1000); // Esperar un segundo antes de imprimir el siguiente número
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("¡Tiempo terminado!");
    }
}
