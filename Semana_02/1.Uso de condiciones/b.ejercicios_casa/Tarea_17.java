import java.util.Scanner;

public class Tarea_17 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        
        System.out.print("Ingrese la cantidad de elementos del vector: ");
        int n = sc.nextInt();
        
        
        int[] vector = new int[n];
        
        
        for (int i = 0; i < n; i++) {
            System.out.print("Ingrese el elemento " + (i+1) + ": ");
            vector[i] = sc.nextInt();
        }
        
        
        int max = vector[0];
        for (int i = 1; i < n; i++) {
            if (vector[i] > max) {
                max = vector[i];
            }
        }
        System.out.println("El valor máximo es: " + max);
        
        
        int min = vector[0];
        for (int i = 1; i < n; i++) {
            if (vector[i] < min) {
                min = vector[i];
            }
        }
        sc.close();
        System.out.println("El valor mínimo es: " + min);
    }
}
