public class Tarea_11 {
    public static void main(String[] args) {
        double montoInicial = 10;
        int meses = 20; 
        double montoMensual = montoInicial; 
        double total = montoInicial; 
        
        
        for (int i = 2; i <= meses; i++) {
            montoMensual *= 2; 
            total += montoMensual; 
        }
        
        
        double pagoMensual = total / meses;
        System.out.println("El pago mensual es: S/ " + pagoMensual);
        System.out.println("El total a pagar después de los " + meses + " meses es: S/ " + total);
    }
}
